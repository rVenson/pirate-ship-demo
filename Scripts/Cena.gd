extends Spatial

export var jogador : NodePath

var perdeu = false
var venceu = false

# Aponta a camera para o jogador e pausa o jogo ao iniciar a cena
func _ready():
	$Camera.target = get_node(jogador).get_node("3DPCamera").get_path()
	$Camera.look = get_node(jogador)
	Engine.time_scale = 0

# Conta a quantidade de inimigos para saber quando a partida foi finalizada
# Atualiza o HUD caso o jogador não seja nulo (tenha sido eliminado)
func _physics_process(delta):
	if $Objetos/Inimigos.get_child_count() <= 0:
		venceu()
	
	if get_node_or_null(jogador) != null:
		$Tela/HUD/BarcoStatus/VidaBarraProgresso.value = $Objetos/Jogador/Barco.vida
		$Tela/HUD/BarcoStatus/VidaBarraProgresso/CanhaoBarraProgresso.value = 100 - $Objetos/Jogador/Barco/TimerCanhao.time_left / 2 * 100

# Executa esta função quando o jogador dispara uma bala no modo bullet time
func _on_Barco_bala_disparada(bala : Node):
	$Camera.target = bala.get_node("3DCamPos").get_path()
	$Camera.look = bala
	Engine.time_scale = 0.5 # Reduz o tempo
	$Camera/CameraTimer.start(1.5) # Inicia um timer para retornar ao estado normal

# Retorna a camera ao jogador e o tempo ao estado normal após o fim do timer
func _on_Timer_timeout():
	$Camera.target = get_node(jogador).get_node("3DPCamera").get_path()
	$Camera.look = get_node(jogador)
	Engine.time_scale = 1

# Executa esta função quando o jogador emite o signal "morte"
func _on_Barco_morte():
	perdeu = true
	$Animacao.play("morte")

# Recarrega a cena ao pressionar o botão "reiniciar" no GUI
func _on_BotaoRestart_pressed():
	get_tree().reload_current_scene()

# Executa a animação para a tela de vitória
func venceu():
	if !perdeu and !venceu:
		venceu = true
		$Animacao.play("vitoria")

# Inicia o jogo, removendo a GUI e normalizando o tempo
func _on_BotaoIniciar_pressed():
	$Animacao.play("inicio")
	Engine.time_scale = 1
